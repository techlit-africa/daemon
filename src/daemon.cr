require "socket"
require "json"

alias IP = Socket::IPAddress

module Daemon
  VERSION = "0.1.0"

  HOST = ENV.fetch("MCAST_HOST", "224.0.3.111").to_s
  SERVER = ENV.fetch("MCAST_SERVER_PORT", "1111").to_i
  CLIENT = ENV.fetch("MCAST_CLIENT_PORT", "1112").to_i

  CONFIG_PATH = ENV.fetch("CONFIG_PATH", "/data/techlit/config.json").to_s

  CLIENT_SEND_INTERVAL = ENV.fetch("SHOUT_INTERVAL", "10").to_i
  SERVER_SEND_INTERVAL = ENV.fetch("SHOUT_INTERVAL", "10").to_i

  CLIENT_TTL = ENV.fetch("CLEAN_INTERVAL", "30").to_i
  SAVE_INTERVAL = ENV.fetch("CLEAN_INTERVAL", "10").to_i

  @[JSON::Serializable::Options(emit_nulls: true)]
  class Config
    include JSON::Serializable

    property role : String
    property version : String

    property hostname : String

    property networkName : String
    property networkPassword : String

    property serverIp : String
    property serverHostname : String

    property clients : Array(ClientStatus)?
  end

  @[JSON::Serializable::Options(emit_nulls: true)]
  class ClientStatus
    include JSON::Serializable

    property lastSeenAt : Time

    property ip : String
    property hostname : String

    property loggedIn : Bool = false
    property guestUser : String? = nil

    property powerStatus : String? = nil
    property powerPercent : String? = nil

    property audioStatus : String? = nil
    property audioPercent : String? = nil

    def initialize
      @lastSeenAt = Time.utc
      @hostname = run "hostname"
      @ip = run "hostname -I | awk '{print $1}'"

      set_power
      set_audio
    end

    private def run(cmd, args = nil)
      stdout = IO::Memory.new
      stderr = IO::Memory.new
      status = Process.run(cmd, args, output: stdout, error: stderr, shell: true)
      raise stderr.to_s.chomp unless status.success?
      stdout.to_s.chomp
    end

    private def set_power
      bat = run "sudo upower -e | grep BAT"
      stat = run "sudo upower -i #{bat}"

      stat.lines.each do |line|
        case line
        when /percentage/
          self.powerPercent = line.split(":")[1].strip

        when /state/
          self.powerStatus = line.split(":")[1].strip
        end
      end

    rescue e
      puts "Error getting power: #{e}"
      self.powerPercent, self.powerStatus = nil, nil
    end

    private def set_audio
      username = run "users | tr ' ' '\n' | grep -v techlit | head -n 1"

      self.guestUser = username.empty? ? nil : username
      self.loggedIn = !!guestUser

      unless self.loggedIn
        self.audioPercent, self.audioStatus = nil, nil
        return
      end

      userid = run "id -u #{username}"

      pactl = "pactl -s /run/user/#{userid}/pulse/native"
      sinks = run "/usr/bin/sudo", ["su", username, "-c", "#{pactl} list sinks"]

      sinks.lines.each do |line|
        line = line.strip
        if line.starts_with?("Volume: ")
          self.audioPercent = line.split(" / ")[1].strip
        elsif line.starts_with?("Mute: ")
          muted = line.split(": ")[1].strip
          self.audioStatus = muted == "no" ? "on" : "off"
        end
      end

    rescue e
      puts "Error getting audio: #{e}"
      self.audioPercent, self.audioStatus = nil, nil
    end
  end

  @@config : Config?

  def self.config
    @@config.not_nil!
  end

  def self.load_config
    @@config = Config.from_json File.read CONFIG_PATH
    puts @@config.to_json
  end

  def self.save_config
    File.write CONFIG_PATH, @@config.to_json
    puts "saving: #{@@config.to_json}"
  end

  def self.receive_from(port, &block : String, IP ->)
    group = IP.new(HOST, port)

    s = UDPSocket.new(Socket::Family::INET)
    s.bind(HOST, port)
    s.join_group(group)

    spawn do
      begin
        puts "Receiving broadcasts from #{HOST}:#{port}"
        loop do
          message, client = s.receive
          block.call(message, client)
        rescue e
          puts e
        end

      ensure
        s.close unless s.nil?
      end
    end
  end

  def self.send_as(port, interval, &block : -> String)
    group = IP.new(HOST, port)

    s = UDPSocket.new(Socket::Family::INET)
    s.bind(HOST, port)
    s.connect(group)

    begin
      puts "Sending broadcasts to #{HOST}:#{port}"
      loop do
        s.send block.call()
        sleep interval
      rescue e
        puts e
      end

    ensure
      s.close unless s.nil?
    end
  end

  def self.run_as_server
    puts "Acting as a TechLit server"

    config.clients ||= [] of ClientStatus

    receive_from CLIENT do |message, addr|
      ip = addr.address.to_s
      puts "#{ip} - #{message}"

      idx = config.clients.not_nil!.index do |client|
        client.ip == ip
      end

      c = ClientStatus.from_json(message)
      c.lastSeenAt = Time.utc
      c.ip = ip

      if idx
        config.clients.not_nil![idx] = c
      else
        config.clients.not_nil!.push(c)
      end
    end

    spawn do
      loop do
        sleep SAVE_INTERVAL
        config.clients.not_nil!.reject! do |client|
          client.lastSeenAt < Time.utc - Time::Span.new(seconds: CLIENT_TTL)
        end
        puts "after clean: #{config.clients.not_nil!.map(&.ip)}"

        save_config
      end
    end

    send_as SERVER, SERVER_SEND_INTERVAL do
      "hello from the server"
    end
  end

  def self.run_as_client
    puts "Acting as a TechLit client"

    config.clients = [] of ClientStatus

    receive_from SERVER do |message, addr|
      ip = addr.address
      puts "#{ip} - #{message}"

      unless config.serverIp == ip
        config.serverIp = ip
        save_config
      end
    end

    send_as CLIENT, CLIENT_SEND_INTERVAL do
      ClientStatus.new.to_json
    end
  end
end

if ARGV.any?
  case ARGV.first
  when "-v", "--version"
    puts "Version #{Daemon::VERSION}"
    exit 1

  when "-h", "--help"
    puts <<-HELP
      TechLit Daemon   # Help message

      -v --version     # Show version
      -h --help        # Show help
      -s --server      # Run as server
      -c --client      # Run as client
    HELP
    exit 1
  end

else
  Daemon.load_config

  if Daemon.config.role == "server"
    Daemon.run_as_server
  else
    Daemon.run_as_client
  end
end
